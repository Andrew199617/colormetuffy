﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[CustomEditor(typeof(AudioManager))]
public class AudioManagerEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        AudioManager audioManager = (AudioManager)target;
        if (GUILayout.Button("Create Audio Sources"))
        {
            audioManager.CreateAudioSources();
        }
    }

}