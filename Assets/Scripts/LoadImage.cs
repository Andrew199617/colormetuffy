﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadImage : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Image image = GetComponent<Image>();
        image.sprite = GameManager.Instance.CurrentPicture;
    }
	
}
