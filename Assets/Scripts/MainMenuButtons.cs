﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtons : MonoBehaviour {

    private IEnumerator LoadScene(string sceneName)
    {
        AudioManager.ButtonPressed();
        yield return new WaitForSeconds(AudioManager.audioManager.ClickLength);
        SceneManager.LoadScene(sceneName);
        yield return null;
    }

    public void StartPressed()
    {
        StartCoroutine(LoadScene("GameScreen"));
    }

    public void AboutPressed()
    {
        StartCoroutine(LoadScene("AboutPage"));
    }

    public void RulesPressed()
    {
        StartCoroutine(LoadScene("RulesPage"));
    }

    public void ContactPressed()
    {
        StartCoroutine(LoadScene("ContactPage"));
    }

}
