﻿using UnityEngine;

public class GameManager : MonoBehaviour {

    public static GameManager Instance;

    public Sprite CurrentPicture;

    [HideInInspector]
    public Color CurrentColor;

    [HideInInspector]
    public int LineWidth;

    void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            Initialize();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Initialize()
    {
        Instance.CurrentColor = new Color(131.0f / 255, 105.0f / 255, 83.0f / 255);
        Instance.LineWidth = 16;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
