﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ColorPicker : MonoBehaviour {

    public GameObject HighlightColor;

    public void ColorPressed()
    {
        AudioManager.ButtonPressed();

        GameObject button = EventSystem.current.currentSelectedGameObject;
        Image image = button.GetComponent<Image>();
        GameManager.Instance.CurrentColor = image.color;

        HighlightColor.transform.SetParent(button.transform.parent);
        HighlightColor.transform.SetAsFirstSibling();
        RectTransform highRect = HighlightColor.GetComponent<RectTransform>();
        RectTransform btnRect = button.GetComponent<RectTransform>();
        highRect.anchorMin = btnRect.anchorMin;
        highRect.anchorMax = btnRect.anchorMax;
        highRect.offsetMin = Vector2.zero;
        highRect.offsetMax = Vector2.zero;
        highRect.anchoredPosition = new UnityEngine.Vector2(0, 0);
    }

    public void EraserPressed()
    {
        AudioManager.ButtonPressed();

        GameObject button = EventSystem.current.currentSelectedGameObject;
        GameManager.Instance.CurrentColor = new Color(0,0,0,0);

        HighlightColor.transform.SetParent(button.transform.parent);
        HighlightColor.transform.SetAsFirstSibling();
        RectTransform highRect = HighlightColor.GetComponent<RectTransform>();
        RectTransform btnRect = button.GetComponent<RectTransform>();
        highRect.anchorMin = btnRect.anchorMin;
        highRect.anchorMax = btnRect.anchorMax;
        highRect.offsetMin = Vector2.zero;
        highRect.offsetMax = Vector2.zero;
        highRect.anchoredPosition = Vector2.zero;
    }

    public void IncreaseLineWidth()
    {
        AudioManager.ButtonPressed();
        if (GameManager.Instance.LineWidth < 24)
        {
            GameManager.Instance.LineWidth += 4;
        }
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).GetComponent<Text>().text = GameManager.Instance.LineWidth.ToString();
    }

    public void DecreaseLineWidth()
    {
        AudioManager.ButtonPressed();
        if (GameManager.Instance.LineWidth > 8)
        {
            GameManager.Instance.LineWidth -= 4;
        }
        EventSystem.current.currentSelectedGameObject.transform.parent.GetChild(0).GetComponent<Text>().text = GameManager.Instance.LineWidth.ToString();
    }

}
