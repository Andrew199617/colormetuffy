﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class ButtonMethods : MonoBehaviour {
    private const string ThatsABeautifulPicture = "That's A Beautiful Picture 01";
    private const string SaveLocationFilename = "D:/test.png";

    public string LastScene;
    public string NextScene;
    public GameObject DialogBox;
    public GameObject DialogTextBox;
    //could also be audioclip and get name
    public List<string> LoadGameSoundEffects = new List<string>();

    public Sprite Dot;

    #region GenericButtons
    
    public void Back()
    {
        StartCoroutine(AudioManager.EButtonPressed(() => LoadScene(LastScene)));
    }

    private void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void LoadGame()
    {
        int soundEffectIndex = Random.Range(0,LoadGameSoundEffects.Count);
        PlaySoundEffect(LoadGameSoundEffects[soundEffectIndex]);

        GameObject button = EventSystem.current.currentSelectedGameObject;
        Image image = button.transform.GetChild(0).GetComponent<Image>();
        GameManager.Instance.CurrentPicture = image.sprite;
        StartCoroutine(AudioManager.EButtonPressed( () => LoadScene(NextScene)) );
    }

    #endregion

    #region GameButtons

    /// <summary>
    /// opens dialog box
    /// </summary>
    public void Save()
    {
        StartCoroutine(AudioManager.EButtonPressed(() =>
        {
            DialogTextBox.SetActive(true);
        }));

    }

    public void SaveNo()
    {
        StartCoroutine(AudioManager.EButtonPressed(() =>
        {
            DialogTextBox.SetActive(false);
        }));
    }

#if UNITY_ANDROID
    public void SaveYes()
    {
        StartCoroutine(AudioManager.EButtonPressed(() =>
        {
            Application.CaptureScreenshot("D:/test.png");
            dialogTextBox.SetActive(false);
        }));
    }
#elif UNITY_WEBGL && !UNITY_EDITOR
    public void SaveYes()
    {
        PlaySoundEffect(ThatsABeautifulPicture);
        DialogTextBox.SetActive(false);
        SaveImage();
    }
    public void SaveImage()
    {
        StartCoroutine(CaptureScreenshot());
    }

    IEnumerator CaptureScreenshot()
    {
        yield return new WaitForEndOfFrame();
        // Create a texture the size of the screen, RGB24 format
        int width = Screen.width - 12;
        int startX = 6;
        int startY = (int)(Screen.height * .25 + 6);
        int endY = (int)(Screen.height * .8 - 6);
        int height = endY - startY;
        
        yield return new WaitForEndOfFrame();

        var tex = new Texture2D(width, height, TextureFormat.RGBA32, false);
    
        // Read screen contents into the texture
        tex.ReadPixels(new Rect(startX, startY, width, height), 0, 0);
        tex.Apply();

        byte[] bytes = tex.EncodeToPNG();
        var bytesBase64 = System.Convert.ToBase64String(bytes);
        Application.ExternalCall("saveImage", bytesBase64);
    
    }
#else

    public void SaveYes()
    {
        PlaySoundEffect(ThatsABeautifulPicture);
        DialogTextBox.SetActive(false);
        SaveImage();
    }

    public void SaveImage()
    {
        StartCoroutine(CaptureScreenshot());
    }

    IEnumerator CaptureScreenshot()
    {
        yield return new WaitForEndOfFrame();
        // Create a texture the size of the screen, RGB24 format
        int width = Screen.width - 12;
        int startX = 6;
        int startY = (int)(Screen.height * .25 + 6);
        int endY = (int)(Screen.height * .8 - 6);
        int height = endY - startY;
        
        yield return new WaitForEndOfFrame();

        var tex = new Texture2D(width, height, TextureFormat.RGBA32, false);

        // Read screen contents into the texture
        tex.ReadPixels(new Rect(startX, startY, width, height), 0, 0);
        tex.Apply();

        byte[] bytes = tex.EncodeToPNG();

        using (System.Drawing.Image image = System.Drawing.Image.FromStream(new System.IO.MemoryStream(bytes)))
        {
            image.Save(SaveLocationFilename, System.Drawing.Imaging.ImageFormat.Png);  // Or Png
        }

    }
#endif


    public void Reset()
    {
        StartCoroutine(AudioManager.EButtonPressed(() =>
        {
            DialogBox.SetActive(true);
        }));
    }

    public void ResetNo()
    {
        StartCoroutine(AudioManager.EButtonPressed(() =>
        {
            DialogBox.SetActive(false);
        }));
    }

    public void ResetYes()
    {
        StartCoroutine(AudioManager.EPlaySoundEffect(
        () => {
            DialogBox.SetActive(false);
        }
        ,"Woop Woop Woop"));
        Drawing.drawing.Reset();
    }

    #endregion

    #region SocialMedia

    public static void OpenUrl(string url)
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        Application.ExternalEval("window.open(\""+ url +"\")");
#else
        Application.OpenURL(url);
#endif
    }

    public void AmazonPressed()
    {
        StartCoroutine(AudioManager.EButtonPressed(() =>
        {
            OpenUrl("https://www.amazon.com/mobile-apps/b?ie=UTF8&node=2350149011&tag=askcomdelta-20");
        }));
    }

    public void GooglePressed()
    {
        StartCoroutine(AudioManager.EButtonPressed(() =>
        {
            OpenUrl("https://play.google.com/store/apps?hl=en");
        }));
    }

    public void ItunesPressed()
    {
        StartCoroutine(AudioManager.EButtonPressed(() =>
        {
            OpenUrl("https://itunes.apple.com/us/genre/ios/id36?mt=8");
        }));
    }

    public void FacebookPressed()
    {
        StartCoroutine(AudioManager.EButtonPressed(() =>
        {
            OpenUrl("https://www.facebook.com/Tuffy-Tiger-399931533450779/");
        }));
    }

    public void PuppetryTwitterPressed()
    {
        StartCoroutine(AudioManager.EButtonPressed(() =>
        {
            OpenUrl("https://twitter.com/puppetryarts");
        }));
    }

    public void AndrewTwitterPressed()
    {
        StartCoroutine(AudioManager.EButtonPressed(() =>
        {
            OpenUrl("https://twitter.com/_AGameDev_");
        }));
    }

    public void PuppetryGamesPressed()
    {
        StartCoroutine(AudioManager.EButtonPressed(() =>
        {
            OpenUrl("http://www.puppetryarts.org/TuffyTiger/#");
        }));
    }

    public void AndrewGamesPressed()
    {
        StartCoroutine(AudioManager.EButtonPressed(() =>
        {
            OpenUrl("http://andrewvelez.net/projects");
        }));
    }

#endregion

#region Sounds

    public AudioSource PlaySoundEffect(string soundEffect)
    {
        return AudioManager.audioManager.GetAndPlaySoundEffect(soundEffect);
    }

#endregion

}
