﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour {

    #region Private Variables

    private const int MaxChildren = 4;

    private readonly List<AudioSource> backgroundMusicSources = new List<AudioSource>();
    private readonly List<AudioSource> soundEffectSources = new List<AudioSource>();
    private AudioSource backgroundMusicSource;
    private AudioSource onClickSource;

    #endregion

    #region Public Variables

#if UNITY_EDITOR
    public List<AudioClip> BackgroundMusicClips = new List<AudioClip>();
    public List<AudioClip> SoundEffectClips = new List<AudioClip>();
    public AudioClip BackgroundMusic;
    public AudioClip OnClick;
#endif

    [SerializeField]
    private string sceneName;

    [NonSerialized]
    public float ClickLength;

    //false merges the two audiomanagers
    public bool ReplaceLastAudioManager;

    public static AudioManager audioManager;

    #endregion

    #region Usable Methods

    public void PlaySoundEffect(string soundEffect)
    {
        if (soundEffectSources.Count > 0)
        {
            AudioSource soundEffectObj = soundEffectSources.Find((audioSource) => audioSource.name == soundEffect);
            if (soundEffectObj)
            {
                soundEffectObj.Play();
            }
            else
            {
                Debug.LogError("Tried to play nonexistent soundeffect: " + soundEffect);
            }
        }
    }

    public AudioSource GetAndPlaySoundEffect(string soundEffect)
    {
        if (soundEffectSources.Count > 0)
        {
            AudioSource soundEffectObj = soundEffectSources.Find((audioSource) => audioSource.name == soundEffect);
            if (soundEffectObj)
            {
                soundEffectObj.Play();
                return soundEffectObj;
            }
        }
        Debug.LogError("Tried to play nonexistent soundeffect: " + soundEffect);
        return null;
    }

    public static void ButtonPressed()
    {
        if (audioManager.onClickSource)
        {
            audioManager.onClickSource.Play();
        }
    }

    public IEnumerator PlayRandomBackgroundMusic()
    {
        if (backgroundMusicSources.Count == 0)
        {
            yield return null;
        }
        while (true)
        {
            int randomChildIndex = UnityEngine.Random.Range(0, backgroundMusicSources.Count);
            AudioSource audioSource = backgroundMusicSources[randomChildIndex];
            audioSource.Play();
            yield return new WaitUntil(() => audioSource.time > audioSource.clip.length - .5f);
        }
    }

    /// <summary>
    /// Waits till click sound is done playing before running your method.
    /// </summary>
    /// <param name="myMethodName">The Method to be called after button clicked or () => { methodbody }</param>
    public static IEnumerator EButtonPressed(Action myMethodName)
    {
        ButtonPressed();
        yield return new WaitForSeconds(audioManager.ClickLength);
        myMethodName();
        yield return null;
    }

    public static IEnumerator EPlaySoundEffect(Action myMethodName, string soundEffect)
    {
        AudioSource source = audioManager.GetAndPlaySoundEffect(soundEffect);
        yield return new WaitForSeconds(source.clip.length);
        myMethodName();
        yield return null;
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// moves audio clips(childs) from oldAudioSources to newAudioSources
    /// </summary>
    /// <param name="oldAudioSources">objects whose clips you'll move to newAudioSources</param>
    /// <param name="newAudioSources"></param>
    private void MigrateAudioSources(Transform oldAudioSources, Transform newAudioSources)
    {
        if (oldAudioSources && newAudioSources)
        {
            for (int i = 0; i < oldAudioSources.childCount; ++i)
            {
                bool hasAudioClip = false;
                foreach (Transform newAudioClip in newAudioSources)
                {
                    if (oldAudioSources.GetChild(i).name == newAudioClip.name)
                    {
                        hasAudioClip = true;
                        break;
                    }
                }
                if (!hasAudioClip)
                {
                    oldAudioSources.GetChild(i).transform.SetParent(newAudioSources.transform);
                    i--;
                }
            }
        }
    }

    void Awake()
    {
        if (!audioManager)
        {
            audioManager = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (ReplaceLastAudioManager)
        {
            Destroy(audioManager.gameObject);
            audioManager = this;
            DontDestroyOnLoad(gameObject);
        }
        else if(sceneName == audioManager.sceneName)
        {
            Destroy(gameObject);
        }
        else
        {
            Transform oldSoundEffects = audioManager.transform.Find("SoundEffects");
            Transform soundEffects = transform.Find("SoundEffects");
            MigrateAudioSources(oldSoundEffects, soundEffects);
            Transform oldBackgroundMusicClips = audioManager.transform.Find("BackgroundMusicSources");
            Transform backgroundMusicClips = transform.Find("BackgroundMusicSources");
            MigrateAudioSources(oldBackgroundMusicClips, backgroundMusicClips);
            Destroy(audioManager.gameObject);
            audioManager = this;
            DontDestroyOnLoad(gameObject);
        }
        
    }

    public void Start()
    {
        GetChildrenAudioSources();
        if (backgroundMusicSource)
        {
            backgroundMusicSource.loop = true;
            backgroundMusicSource.Play();
        }
        else
        {
            StartCoroutine(PlayRandomBackgroundMusic());
        }
    }

    private void GetChildrenAudioSources()
    {
        backgroundMusicSources.Clear();
        soundEffectSources.Clear();
        foreach (Transform child in transform)
        {
            switch (child.name)
            {
                case "BackgroundMusicSources":
                    foreach (Transform backgroundMusicChild in child)
                    {
                        backgroundMusicSources.Add(backgroundMusicChild.GetComponent<AudioSource>());
                    }
                    break;
                case "SoundEffects":
                    foreach (Transform soundEffectChild in child)
                    {
                        soundEffectSources.Add(soundEffectChild.GetComponent<AudioSource>());
                    }
                    break;
                case "BackgroundMusic":
                    backgroundMusicSource = child.GetComponent<AudioSource>();
                    break;
                case "OnClick":
                    onClickSource = child.GetComponent<AudioSource>();
                    ClickLength = onClickSource.clip.length;
                    break;
            }
        }
    }


    private void AddAudioClips(Transform containerTransform, List<AudioClip> audioClips)
    {
        foreach (AudioClip audioClipChild in audioClips)
        {
            GameObject obj = new GameObject(audioClipChild.name);
            obj.transform.SetParent(containerTransform);
            AudioSource newAudioSource = obj.AddComponent<AudioSource>();
            newAudioSource.clip = audioClipChild;
            newAudioSource.playOnAwake = false;
        }
    }


    private void CreateAudioClipContainer(string container, List<AudioClip> audioClips)
    {
        Transform oldContainerTransform = transform.Find(container);
        if (oldContainerTransform)
        {
            DestroyImmediate(oldContainerTransform.gameObject);
        }

        GameObject containerObject = new GameObject(container);
        containerObject.transform.SetParent(transform);
        AddAudioClips(containerObject.transform, audioClips);
    }

    private void DestroyChildren()
    {
        if (transform.childCount > MaxChildren)
        {
            foreach (Transform child in transform)
            {
                DestroyImmediate(child.gameObject);
            }
        }
    }

#if UNITY_EDITOR

    public void CreateAudioSources()
    {
        DestroyChildren();
        GetChildrenAudioSources();

        if (soundEffectSources.Count > 0)
        {
            CreateAudioClipContainer("SoundEffects", SoundEffectClips);
        }
        if (backgroundMusicSources.Count > 0)
        {
            CreateAudioClipContainer("BackgroundMusicSources", BackgroundMusicClips);
        }
        ReplaceAudioClip("BackgroundMusic", BackgroundMusic);
        ReplaceAudioClip("OnClick", OnClick);
    }

    /// <summary>
    /// used for single audio sources
    /// </summary>
    /// <param name="audioSourceName">Name of audio source</param>
    /// <param name="newAudioClip">Audio clip to check for</param>
    private void ReplaceAudioClip(string audioSourceName, AudioClip newAudioClip)
    {
        if (newAudioClip == null)
        {
            return;
        }
        Transform oldAudioSourceTransform = transform.Find(audioSourceName);

        if (oldAudioSourceTransform == null)
        {
            GameObject obj = new GameObject(audioSourceName);
            obj.transform.SetParent(transform);
            AudioSource audioSource = obj.AddComponent<AudioSource>();
            audioSource.clip = newAudioClip;
            audioSource.playOnAwake = false;
        }
        else
        {
            AudioSource oldAudioSource = oldAudioSourceTransform.GetComponent<AudioSource>();
            oldAudioSource.clip = newAudioClip;
        }
    }

#endif

    #endregion

}
