﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Drawing : MonoBehaviour {

    #region Public Variables

    public static Drawing drawing;

    public GameObject Dot;

    public int DotsPerRow;

    #endregion

    #region Private Variables

    private int dotsCount;

    private readonly List<Image> dotImages = new List<Image>();

    private readonly Color transparent = new Color(0, 0, 0, 0);

    //private readonly List<Vector2> dotPositions = new List<Vector2>();
    private List<Rect> firstDivision;
    private readonly List<List<Rect>> secondDivision = new List<List<Rect>>();
    private readonly List<List<List<Vector2>>> dotPositions = new List<List<List<Vector2>>>();
    private readonly List<List<List<int>>> sibilingIndexs = new List<List<List<int>>>();

    private int sibilingIndex;
    private float dotWidth;
    private float dotHeight;


    #endregion

    private enum LengthHeight
    {
        One = 0,
        Height3Cross = 1,
        Height5Cross = 2,
        Height5Circle = 3,
        Height7Circle = 4,
    }

    public void Awake()
    {
        //TODO: drawing should not load twice like audiomanager
        if (!drawing)
        {
            Initialize();
            SpawnDots();
            drawing = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Length3(int siblingIndex)
    {
        Length(siblingIndex, 1);
    }

    private void Length5(int siblingIndex)
    {
        Length(siblingIndex, 2);
    }

    private void Length7(int siblingIndex)
    {
        Length(siblingIndex, 3);
    }
    private void Length9(int siblingIndex)
    {
        Length(siblingIndex, 4);
    }

    private void Length(int siblingIndex, int numDots)
    {
        int mod = siblingIndex % DotsPerRow;
        for (int i = 1; i <= numDots; ++i)
        {
            if (mod < DotsPerRow - i)
            {
                ShowDot(siblingIndex + i);
            }
            if (mod >= i)
            {
                ShowDot(siblingIndex - i);
            }
        }
        ShowDot(siblingIndex);
    }

    private void ShowDot(int siblingIndex)
    {
        if(siblingIndex >= 0 && siblingIndex < dotsCount)
        {
            dotImages[siblingIndex].color = GameManager.Instance.CurrentColor;
        }
    }

    private void ShowDots(LengthHeight size)
    {
        if(sibilingIndex == -1) { return; }
        switch (size)
        {
            case LengthHeight.Height5Cross:
                Length3(sibilingIndex + 3 * DotsPerRow);
                Length5(sibilingIndex + 2 * DotsPerRow);
                Length7(sibilingIndex + DotsPerRow);
                Length7(sibilingIndex);
                Length7(sibilingIndex - DotsPerRow);
                Length5(sibilingIndex - 2 * DotsPerRow);
                Length3(sibilingIndex - 3 * DotsPerRow);
                break;
            case LengthHeight.Height3Cross:
                ShowDot(sibilingIndex + 2 * DotsPerRow);
                Length3(sibilingIndex + DotsPerRow);
                Length5(sibilingIndex);
                Length3(sibilingIndex - DotsPerRow);
                ShowDot(sibilingIndex - 2 * DotsPerRow);
                break;
            case LengthHeight.One:
                ShowDot(sibilingIndex + DotsPerRow);
                Length3(sibilingIndex);
                ShowDot(sibilingIndex - DotsPerRow);
                break;
            case LengthHeight.Height5Circle:
                Length5(sibilingIndex + 3 * DotsPerRow);
                Length7(sibilingIndex + 2 * DotsPerRow);
                Length7(sibilingIndex + DotsPerRow);
                Length7(sibilingIndex);
                Length7(sibilingIndex - DotsPerRow);
                Length7(sibilingIndex - 2 * DotsPerRow);
                Length5(sibilingIndex - 3 * DotsPerRow);
                break;
            case LengthHeight.Height7Circle:
                Length5(sibilingIndex + 4 * DotsPerRow);
                Length7(sibilingIndex + 3 * DotsPerRow);
                Length9(sibilingIndex + 2 * DotsPerRow);
                Length9(sibilingIndex + DotsPerRow);
                Length9(sibilingIndex);
                Length9(sibilingIndex - DotsPerRow);
                Length9(sibilingIndex - 2 * DotsPerRow);
                Length7(sibilingIndex - 3 * DotsPerRow);
                Length5(sibilingIndex - 4 * DotsPerRow);
                break;
        }

    }

    // Update is called once per frame
    public void Update ()
    {
        if (Input.GetMouseButton(0))
        {
            //if button was pressed dont draw
            if (EventSystem.current.currentSelectedGameObject == null)
            {
                GetCurrentDot();
                var size = (GameManager.Instance.LineWidth - 8) / 4;
                ShowDots((LengthHeight)size);
            }
        }
    }

    /// <summary>
    /// Sets the current;y selected dot using spatial partitioning.
    /// </summary>
    private void GetCurrentDot()
    {
        var inputMousePosition = Input.mousePosition + new Vector3(dotWidth , -dotHeight);
        int firstIndex = firstDivision.FindIndex((rect) => rect.Contains(inputMousePosition));
        if (firstIndex != -1)
        {
            int secondIndex = secondDivision[firstIndex].FindIndex((rect) => rect.Contains(inputMousePosition));
            if (secondIndex != -1)
            {
                var sibIndex = dotPositions[firstIndex][secondIndex].FindIndex(
                    (vec2) => vec2.x + dotWidth > inputMousePosition.x && vec2.x - dotWidth < inputMousePosition.x
                              && vec2.y + dotHeight > inputMousePosition.y && vec2.y - dotHeight < inputMousePosition.y
                );
                if (sibIndex != -1)
                {
                    sibilingIndex = sibilingIndexs[firstIndex][secondIndex][sibIndex];
                    return;
                }
            }
        }
        sibilingIndex = -1;
    }

    private void SpawnDots()
    {
        Canvas canvas = FindObjectOfType<Canvas>();
        DivideScreen(canvas);

        var increment = 1.0f / DotsPerRow;
        var currentMinX = 0.0f;
        var currentMaxX = increment;
        var currentMinY = 1.0f - increment;
        var currentMaxY = 1.0f;
        var dotsInRow = 0;
        while (true)
        {
            var obj = Instantiate(Dot, transform);
            obj.transform.SetSiblingIndex(dotsCount);
            dotsCount++;

            RectTransform rectTransform = obj.GetComponent<RectTransform>();
            rectTransform.anchorMin = new Vector2(currentMinX, currentMinY);
            rectTransform.anchorMax = new Vector2(currentMaxX, currentMaxY);

            Vector2 dotPosition = RectTransformUtility.PixelAdjustPoint(rectTransform.position, obj.transform, canvas);
            int firstIndex = firstDivision.FindIndex((rect) => rect.Contains(dotPosition));
            if (firstIndex != -1)
            {
                int secondIndex = secondDivision[firstIndex].FindIndex((rect) => rect.Contains(dotPosition));

                if (secondIndex != -1)
                {
                    dotPositions[firstIndex][secondIndex].Add(dotPosition);
                    sibilingIndexs[firstIndex][secondIndex].Add(dotsCount - 1);
                }
            }
            

            Image image = obj.GetComponent<Image>();
            image.color = new Color(0, 0, 0, 0);
            
            dotImages.Insert(dotsCount - 1, image);
            dotsInRow++;

            currentMinX += increment;
            currentMaxX += increment;
            if (dotsInRow == DotsPerRow)
            {
                dotsInRow = 0;
                currentMinX = 0;
                currentMaxX = increment;
                currentMinY -= increment;
                currentMaxY -= increment;
                if (currentMaxY <= 0)
                {
                    dotWidth = rectTransform.rect.width;
                    dotHeight = rectTransform.rect.height;
                    break;
                }
            }
        }
    }

    private void Initialize()
    {
        dotsCount = 0;
        for (int i = 0; i < 9; ++i)
        {
            dotPositions.Add(new List<List<Vector2>>());
            for (int j = 0; j < 9; ++j)
            {
                dotPositions[i].Add(new List<Vector2>());
            }
        }
        for (int i = 0; i < 9; ++i)
        {
            sibilingIndexs.Add(new List<List<int>>());
            for (int j = 0; j < 9; ++j)
            {
                sibilingIndexs[i].Add(new List<int>());
            }
        }
    }

    private void DivideScreen(Canvas canvas)
    {
        RectTransform rectTransform = transform.GetComponent<RectTransform>();
        Vector2 recttest = RectTransformUtility.PixelAdjustPoint(rectTransform.position, transform, canvas);
        firstDivision = SubdivideRect(recttest, rectTransform.rect);
        foreach (var rect in firstDivision)
        {
            secondDivision.Add(SubdivideRect(RectTransformUtility.PixelAdjustPoint(rect.center, transform, canvas), rect));
        }
    }

    /// <summary>
    /// Takes a rect and divides into 9 rects
    /// </summary>
    /// <param name="position">Center of Rect</param>
    /// <param name="rect"></param>
    /// <returns></returns>
    private List<Rect> SubdivideRect(Vector2 position,Rect rect)
    {
        float leftX = position.x - rect.width / 2;
        float topY = position.y - rect.height / 2;
        float thirdWidth = rect.width / 3;
        float thirdHeight = rect.height / 3;

        List<Rect> rects = new List<Rect>();

        for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                rects.Add(new Rect(leftX + thirdWidth * i, topY + thirdHeight * j, thirdWidth, thirdHeight));
            }
        }

        return rects;
    }

    public void Reset()
    {
        foreach (Image image in dotImages)
        {
            image.color = transparent;
        }
    }

    public void ReloadDots(Sprite sprite)
    {
        foreach (Image image in dotImages)
        {
            image.sprite = sprite;
        }
    }

}
